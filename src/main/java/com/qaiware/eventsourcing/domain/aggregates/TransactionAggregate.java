package com.qaiware.eventsourcing.domain.aggregates;

import com.qaiware.eventsourcing.domain.commands.CreateTransactionCommand;
import com.qaiware.eventsourcing.domain.commands.CreditTransactionCommand;
import com.qaiware.eventsourcing.domain.commands.DebitTransactionCommand;
import com.qaiware.eventsourcing.domain.commands.validation.rules.DebitTransactionValidationRule;
import com.qaiware.eventsourcing.domain.commands.validation.rules.PositiveAmountValidationRule;
import com.qaiware.eventsourcing.domain.events.TransactionCreatedEvent;
import com.qaiware.eventsourcing.domain.events.TransactionCreditedEvent;
import com.qaiware.eventsourcing.domain.events.TransactionDebitFailedEvent;
import com.qaiware.eventsourcing.domain.events.TransactionDebitedEvent;
import com.qaiware.eventsourcing.eventstore.aggregate.AbstractAggregate;
import com.qaiware.eventsourcing.eventstore.command.Command;
import com.qaiware.eventsourcing.eventstore.command.validation.CommandValidator;
import com.qaiware.eventsourcing.eventstore.event.Event;
import java.math.BigDecimal;

public class TransactionAggregate extends AbstractAggregate<TransactionAggregate, Command> {

  private BigDecimal amount;

  public BigDecimal getAmount() {

    return this.amount;
  }

  public void setAmount(BigDecimal amount) {

    this.amount = amount;
  }

  // Command handlers
  // =============================

  public Event process(CreateTransactionCommand cmd) {

    return TransactionCreatedEvent.create(cmd.getInitialAmount());
  }

  public Event process(CreditTransactionCommand cmd) {
    CommandValidator commandValidator = CommandValidator
        .create(cmd, this, TransactionCreditedEvent.create(cmd.getAmount()))
        .addRule(PositiveAmountValidationRule.create());

    return commandValidator.run();
  }

  public Event process(DebitTransactionCommand cmd) {
    CommandValidator commandValidator = CommandValidator
        .create(cmd, this, TransactionDebitedEvent.create(cmd.getAmount()))
        .addRule(PositiveAmountValidationRule.create())
        .addRule(
            DebitTransactionValidationRule.create(
                TransactionDebitFailedEvent.create("Insufficient amount!")
            )
        );

    return commandValidator.run();
  }

  // Event handlers
  // =============================

  public TransactionAggregate apply(TransactionCreatedEvent event) {

    this.amount = event.getInitialAmount();

    return this;
  }

  public TransactionAggregate apply(TransactionCreditedEvent event) {

    this.amount = this.amount.add(event.getAmount());

    return this;
  }

  public TransactionAggregate apply(TransactionDebitedEvent event) {

    this.amount = this.amount.subtract(event.getAmount());

    return this;
  }

  // TODO: Important note:
  // Every event which can be emitted MUST have an apply method.
  // Otherwise, NoSuchMethodException is thrown!
  public TransactionAggregate apply(TransactionDebitFailedEvent event) {
    // An empty event handler
    return this;
  }

}
