package com.qaiware.eventsourcing.domain.aggregates;

import com.qaiware.eventsourcing.eventstore.aggregate.AbstractAggregate;
import com.qaiware.eventsourcing.eventstore.aggregate.AggregateClassMapper;
import java.util.HashMap;
import org.springframework.stereotype.Component;

@Component
public class UpiClassMapper implements AggregateClassMapper {

  private static final HashMap<String, Class<? extends AbstractAggregate>> classMap = new HashMap<>();

  static {
    classMap.put("Transaction", TransactionAggregate.class);
  }

  @Override
  public Class<? extends AbstractAggregate> getAggregateClassByType(String aggregateType) {

    if (classMap.containsKey(aggregateType)) {
      return classMap.get(aggregateType);
    }

    throw new IllegalArgumentException(
        ("Aggregate type " + aggregateType + " is not supported. Nice try :)"));

  }

  @Override
  public String getTypeByAggregateClass(Class<? extends AbstractAggregate> clazz) {

    return classMap
        .keySet()
        .stream()
        .filter(key -> classMap.get(key).equals(clazz))
        .findFirst()
        .orElseThrow(() -> new IllegalArgumentException(
            "Aggregate class " + clazz.getName() + " means nothing to me."));
  }
}
