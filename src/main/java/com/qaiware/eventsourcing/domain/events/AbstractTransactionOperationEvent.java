package com.qaiware.eventsourcing.domain.events;

import com.qaiware.eventsourcing.eventstore.event.Event;
import java.math.BigDecimal;

public abstract class AbstractTransactionOperationEvent implements Event {

  private BigDecimal amount;

  protected AbstractTransactionOperationEvent() {

  }

  protected AbstractTransactionOperationEvent(BigDecimal amount) {

    this.amount = amount;
  }

  public BigDecimal getAmount() {

    return this.amount;
  }

  public void setAmount(BigDecimal amount) {

    this.amount = amount;
  }
}
