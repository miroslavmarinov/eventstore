package com.qaiware.eventsourcing.domain.events;

import java.math.BigDecimal;

public class TransactionCreditedEvent extends AbstractTransactionOperationEvent {

  private TransactionCreditedEvent() {

  }

  private TransactionCreditedEvent(BigDecimal amount) {

    super(amount);
  }

  public static TransactionCreditedEvent create(BigDecimal amount) {

    return new TransactionCreditedEvent(amount);
  }
}
