package com.qaiware.eventsourcing.domain.events;

import java.math.BigDecimal;

public class TransactionDebitedEvent extends AbstractTransactionOperationEvent {

  private TransactionDebitedEvent() {

  }

  private TransactionDebitedEvent(BigDecimal amount) {

    super(amount);
  }

  public static TransactionDebitedEvent create(BigDecimal amount) {

    return new TransactionDebitedEvent(amount);
  }
}
