package com.qaiware.eventsourcing.domain.events;

import com.qaiware.eventsourcing.eventstore.event.Event;
import java.math.BigDecimal;

public class TransactionCreatedEvent implements Event {

  private BigDecimal initialAmount;

  private TransactionCreatedEvent() {

  }

  private TransactionCreatedEvent(BigDecimal initialAmount) {

    this.initialAmount = initialAmount;
  }

  public BigDecimal getInitialAmount() {

    return this.initialAmount;
  }

  public void setInitialAmount(BigDecimal initialAmount) {

    this.initialAmount = initialAmount;
  }

  public static TransactionCreatedEvent create(BigDecimal initialAmount) {

    return new TransactionCreatedEvent(initialAmount);
  }

}
