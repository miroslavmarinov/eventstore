package com.qaiware.eventsourcing.domain.events;

import com.qaiware.eventsourcing.eventstore.event.Event;

public class TransactionDebitFailedEvent implements Event {

  private String reason;

  private TransactionDebitFailedEvent() {

  }

  private TransactionDebitFailedEvent(String reason) {
    this.reason = reason;
  }

  public static TransactionDebitFailedEvent create(String reason) {
    return new TransactionDebitFailedEvent(reason);
  }

  public String getReason() {
    return this.reason;
  }

}
