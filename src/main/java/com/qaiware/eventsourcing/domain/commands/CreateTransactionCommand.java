package com.qaiware.eventsourcing.domain.commands;

import com.qaiware.eventsourcing.eventstore.command.Command;
import java.math.BigDecimal;

public class CreateTransactionCommand implements Command {

  private BigDecimal initialAmount;

  private CreateTransactionCommand(BigDecimal initialAmount) {

    this.initialAmount = initialAmount;
  }

  public BigDecimal getInitialAmount() {

    return this.initialAmount;
  }

  public void setInitialAmount(BigDecimal initialAmount) {

    this.initialAmount = initialAmount;
  }

  public static CreateTransactionCommand create(BigDecimal initialAmount) {

    return new CreateTransactionCommand(initialAmount);
  }
}
