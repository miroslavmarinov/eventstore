package com.qaiware.eventsourcing.domain.commands;

import com.qaiware.eventsourcing.eventstore.command.Command;
import java.math.BigDecimal;

public abstract class AbstractTransactionOperationCommand implements Command {

  private BigDecimal amount;

  protected AbstractTransactionOperationCommand(BigDecimal amount) {

    this.amount = amount;
  }

  public BigDecimal getAmount() {

    return this.amount;
  }

  public void setAmount(BigDecimal amount) {

    this.amount = amount;
  }

}
