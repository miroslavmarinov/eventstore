package com.qaiware.eventsourcing.domain.commands;

import java.math.BigDecimal;

public class DebitTransactionCommand extends AbstractTransactionOperationCommand {

  protected DebitTransactionCommand(BigDecimal amount) {

    super(amount);
  }

  public static DebitTransactionCommand create(BigDecimal amount) {

    return new DebitTransactionCommand(amount);
  }

}
