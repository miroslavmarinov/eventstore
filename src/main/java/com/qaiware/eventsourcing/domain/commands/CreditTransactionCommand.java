package com.qaiware.eventsourcing.domain.commands;

import java.math.BigDecimal;

public class CreditTransactionCommand extends AbstractTransactionOperationCommand {

  protected CreditTransactionCommand(BigDecimal amount) {

    super(amount);
  }

  public static CreditTransactionCommand create(BigDecimal amount) {

    return new CreditTransactionCommand(amount);
  }

}
