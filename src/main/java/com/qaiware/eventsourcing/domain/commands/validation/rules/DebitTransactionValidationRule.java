package com.qaiware.eventsourcing.domain.commands.validation.rules;

import com.qaiware.eventsourcing.domain.aggregates.TransactionAggregate;
import com.qaiware.eventsourcing.domain.commands.DebitTransactionCommand;
import com.qaiware.eventsourcing.eventstore.command.validation.ValidationError;
import com.qaiware.eventsourcing.eventstore.command.validation.rule.ValidationRule;
import com.qaiware.eventsourcing.eventstore.command.validation.rule.ValidationRuleResult;
import com.qaiware.eventsourcing.eventstore.event.Event;

public class DebitTransactionValidationRule extends
    ValidationRule<DebitTransactionCommand, TransactionAggregate> {

  private DebitTransactionValidationRule(Event... failEvents) {
    super(failEvents);
  }

  public static DebitTransactionValidationRule create(Event... failEvents) {
    return new DebitTransactionValidationRule(failEvents);
  }

  @Override
  public ValidationRuleResult check(
      DebitTransactionCommand command,
      TransactionAggregate aggregate
  ) {
    if (aggregate.getAmount().compareTo(command.getAmount()) == -1) {
      return ValidationRuleResult.create(
          ValidationError.create(this, "Insufficient amount! How embarrassing :)")
      );
    }

    return ValidationRuleResult.create();
  }

}
