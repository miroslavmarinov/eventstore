package com.qaiware.eventsourcing.domain.commands.validation.rules;

import com.qaiware.eventsourcing.domain.aggregates.TransactionAggregate;
import com.qaiware.eventsourcing.domain.commands.AbstractTransactionOperationCommand;
import com.qaiware.eventsourcing.domain.commands.CreditTransactionCommand;
import com.qaiware.eventsourcing.domain.commands.DebitTransactionCommand;
import com.qaiware.eventsourcing.eventstore.command.validation.ValidationError;
import com.qaiware.eventsourcing.eventstore.command.validation.rule.ValidationRule;
import com.qaiware.eventsourcing.eventstore.command.validation.rule.ValidationRuleResult;
import com.qaiware.eventsourcing.eventstore.event.Event;
import java.math.BigDecimal;

public class PositiveAmountValidationRule extends
    ValidationRule<AbstractTransactionOperationCommand, TransactionAggregate> {

  private PositiveAmountValidationRule(Event... failEvents) {
    super(failEvents);
  }

  public static PositiveAmountValidationRule create(Event... failEvents) {
    return new PositiveAmountValidationRule(failEvents);
  }

  @Override
  public ValidationRuleResult check(
      AbstractTransactionOperationCommand command,
      TransactionAggregate aggregate
  ) {
    if (command.getAmount().compareTo(BigDecimal.ZERO) != 1) {
      StringBuilder errorMessage = new StringBuilder("The ");

      if (command instanceof DebitTransactionCommand) {
        errorMessage.append("debit ");
      } else if (command instanceof CreditTransactionCommand) {
        errorMessage.append("credit ");
      }

      errorMessage.append("amount cannot be negative or zero!");

      return ValidationRuleResult.create(
          ValidationError.create(this, errorMessage.toString())
      );
    }

    return ValidationRuleResult.create();
  }

}
