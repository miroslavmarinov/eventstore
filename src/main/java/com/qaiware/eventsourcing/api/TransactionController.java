package com.qaiware.eventsourcing.api;

import com.qaiware.eventsourcing.api.json.CreateTransactionRequest;
import com.qaiware.eventsourcing.api.json.CreditTransactionRequest;
import com.qaiware.eventsourcing.api.json.DebitTransactionRequest;
import com.qaiware.eventsourcing.api.json.TransactionResponse;
import com.qaiware.eventsourcing.service.TransactionService;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/transactions")
public class TransactionController {

  @Autowired
  private TransactionService transactionService;

  @PostMapping
  public ResponseEntity<TransactionResponse> create(
      @RequestBody @Valid CreateTransactionRequest createTransactionRequest
  ) {

    return ResponseEntity
        .status(HttpStatus.CREATED)
        .body(this.transactionService.create(createTransactionRequest));
  }

  @PostMapping(path = "/{id}/credit")
  public ResponseEntity<TransactionResponse> credit(
      @PathVariable(name = "id") String transactionId,
      @RequestBody @Valid CreditTransactionRequest creditTransactionRequest
  ) {

    return ResponseEntity
        .status(HttpStatus.CREATED)
        .body(this.transactionService.credit(transactionId, creditTransactionRequest));
  }

  @PostMapping(path = "/{id}/debit")
  public ResponseEntity<TransactionResponse> debit(
      @PathVariable(name = "id") String transactionId,
      @RequestBody @Valid DebitTransactionRequest debitTransactionRequest
  ) {

    return ResponseEntity
        .status(HttpStatus.CREATED)
        .body(this.transactionService.debit(transactionId, debitTransactionRequest));
  }

}
