package com.qaiware.eventsourcing.api.json;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.validation.constraints.NotEmpty;

public class CreditTransactionRequest {

  @JsonProperty(value = "amount")
  @NotEmpty
  private String amount;

  public String getAmount() {

    return this.amount;
  }

  public void setAmount(String amount) {

    this.amount = amount;
  }
}
