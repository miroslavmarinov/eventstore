package com.qaiware.eventsourcing.api.json;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;
import java.util.UUID;

public class TransactionResponse {

  @JsonProperty(value = "transaction_id")
  private UUID transactionId;

  @JsonProperty(value = "amount")
  private BigDecimal amount;

  private TransactionResponse() {

  }

  public UUID getTransactionId() {

    return this.transactionId;
  }

  public void setTransactionId(UUID transactionId) {

    this.transactionId = transactionId;
  }

  public BigDecimal getAmount() {

    return this.amount;
  }

  public void setAmount(BigDecimal amount) {

    this.amount = amount;
  }

  public static TransactionResponse create(UUID transactionId, BigDecimal amount) {

    TransactionResponse response = new TransactionResponse();
    response.setTransactionId(transactionId);
    response.setAmount(amount);
    return response;
  }

}
