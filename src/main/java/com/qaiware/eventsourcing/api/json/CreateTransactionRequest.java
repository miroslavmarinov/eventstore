package com.qaiware.eventsourcing.api.json;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.validation.constraints.NotEmpty;

public class CreateTransactionRequest {

  @JsonProperty(value = "initial_amount")
  @NotEmpty
  private String initialAmount;

  public String getInitialAmount() {

    return this.initialAmount;
  }

  public void setInitialAmount(String initialAmount) {

    this.initialAmount = initialAmount;
  }
}
