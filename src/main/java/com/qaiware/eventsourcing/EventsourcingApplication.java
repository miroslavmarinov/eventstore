package com.qaiware.eventsourcing;

import com.qaiware.eventsourcing.domain.aggregates.TransactionAggregate;
import com.qaiware.eventsourcing.eventstore.aggregate.DummyEventPublisher;
import com.qaiware.eventsourcing.eventstore.aggregate.EventStore;
import com.qaiware.eventsourcing.eventstore.command.Command;
import com.qaiware.eventsourcing.eventstore.repository.AggregateRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class EventsourcingApplication {

  @Bean
  public static AggregateRepository<TransactionAggregate, Command> transactionRepository(
      EventStore eventStore, DummyEventPublisher eventPublisher) {

    return new AggregateRepository<>(TransactionAggregate.class, eventStore, eventPublisher);
  }

  public static void main(String[] args) {

    SpringApplication.run(EventsourcingApplication.class, args);
  }

}
