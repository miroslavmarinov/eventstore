package com.qaiware.eventsourcing.eventstore.command.validation;

import com.qaiware.eventsourcing.eventstore.command.Command;
import com.qaiware.eventsourcing.eventstore.command.validation.rule.ValidationRule;
import com.qaiware.eventsourcing.eventstore.command.validation.rule.ValidationRuleResult;
import com.qaiware.eventsourcing.eventstore.event.Event;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CommandValidator {

  private static final Logger log = LoggerFactory.getLogger(CommandValidator.class);

  private final Command command;

  private final Object aggregate;

  private final List<ValidationRule> validationRules;

  private List<Event> resultEvents;

  public Event run() {
    List<ValidationError> errors = new ArrayList<>();

    for (ValidationRule rule : this.validationRules) {
      ValidationRuleResult validationRuleResult = rule.check(this.command, this.aggregate);

      if (validationRuleResult.containsErrors()) {
        errors.addAll(validationRuleResult.getErrors());
        this.setResultEvents(null);

        if (validationRuleResult.emitsFailEvents()) {
          this.setResultEvents(validationRuleResult.getFailEvents());
        }
        break;
      }
    }

    ValidationResult validationResult = ValidationResult.create(this.getResultEvents(), errors);

    return this.processValidationResult(validationResult);
  }

  // TODO: Discuss:
  // 1. Is it possible to emit more than one event (currently taking the first one - get(0))?
  // 2. If there is no fail event, an exception is thrown?
  // 3. In case of fail event, should we throw an exception?
  private Event processValidationResult(ValidationResult validationResult) {
    String errorMessage = validationResult
        .getErrors()
        .stream()
        .map(ValidationError::getMessage)
        .collect(Collectors.joining(",\r\n"));

    if (validationResult.emitsEvents()) {
      if (validationResult.containsErrors()) {
        CommandValidator.log.warn(errorMessage);
      }
      return validationResult.getResultEvents().get(0);
    } else if (validationResult.containsErrors() && !validationResult.emitsEvents()) {
      throw new CommandValidationException(errorMessage);
    } else {
      throw new IllegalStateException("Command handling should result with event and/or error");
    }
  }

  private CommandValidator(
      Command command,
      Object aggregate,
      Event onSuccessEvent
  ) {
    this.command = command;
    this.aggregate = aggregate;
    this.validationRules = new ArrayList<>();

    this.resultEvents = new ArrayList<>();
    this.resultEvents.add(onSuccessEvent);
  }

  private CommandValidator(
      Command command,
      Object aggregate,
      Event... onSuccessEvents
  ) {
    this.command = command;
    this.aggregate = aggregate;
    this.validationRules = new ArrayList<>();
    this.resultEvents = Arrays.asList(onSuccessEvents);
  }

  public static CommandValidator create(
      Command command,
      Object aggregate,
      Event onSuccessEvent
  ) {
    return new CommandValidator(command, aggregate, onSuccessEvent);
  }

  public static CommandValidator create(
      Command command,
      Object aggregate,
      Event... onSuccessEvents
  ) {
    return new CommandValidator(command, aggregate, onSuccessEvents);
  }

  public CommandValidator addRule(ValidationRule validationRule) {
    this.validationRules.add(validationRule);
    return this;
  }

  private List<Event> getResultEvents() {
    return this.resultEvents;
  }

  private void setResultEvents(List<Event> resultEvents) {
    this.resultEvents = resultEvents;
  }

}
