package com.qaiware.eventsourcing.eventstore.command.validation;


import com.qaiware.eventsourcing.eventstore.event.Event;
import java.util.List;

public class ValidationResult {

  private final List<Event> resultEvents;

  private final List<ValidationError> errors;

  private ValidationResult(
      List<Event> resultEvents,
      List<ValidationError> errors
  ) {
    this.resultEvents = resultEvents;
    this.errors = errors;
  }

  public static ValidationResult create(
      List<Event> resultEvents,
      List<ValidationError> errors
  ) {
    return new ValidationResult(resultEvents, errors);
  }

  public boolean emitsEvents() {
    return (this.resultEvents != null && !this.resultEvents.isEmpty());
  }

  public boolean containsErrors() {
    return (this.errors != null && !this.errors.isEmpty());
  }

  public List<Event> getResultEvents() {
    return this.resultEvents;
  }

  public List<ValidationError> getErrors() {
    return this.errors;
  }

}
