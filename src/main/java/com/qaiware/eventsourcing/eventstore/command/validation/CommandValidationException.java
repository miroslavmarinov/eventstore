package com.qaiware.eventsourcing.eventstore.command.validation;

public class CommandValidationException extends RuntimeException {

  private static final long serialVersionUID = 4616844833294562053L;

  public CommandValidationException(String message) {
    super(message);
  }

}
