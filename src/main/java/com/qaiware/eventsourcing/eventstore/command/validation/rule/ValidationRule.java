package com.qaiware.eventsourcing.eventstore.command.validation.rule;

import com.qaiware.eventsourcing.eventstore.command.Command;
import com.qaiware.eventsourcing.eventstore.event.Event;
import java.util.Arrays;
import java.util.List;

public abstract class ValidationRule<C extends Command, A> {

  private final List<Event> failEvents;

  public ValidationRule(Event... failEvents) {
    this.failEvents = Arrays.asList(failEvents);
  }

  public abstract ValidationRuleResult check(C command, A aggregate);

  public List<Event> getFailEvents() {
    return this.failEvents;
  }

}
