package com.qaiware.eventsourcing.eventstore.command.validation;

import com.qaiware.eventsourcing.eventstore.command.validation.rule.ValidationRule;

public class ValidationError {

  private final ValidationRule violatedRule;

  private final String message;

  private ValidationError(
      ValidationRule violatedRule,
      String message
  ) {
    this.violatedRule = violatedRule;
    this.message = message;
  }

  public static ValidationError create(
      ValidationRule violatedRule,
      String message
  ) {
    return new ValidationError(violatedRule, message);
  }

  public ValidationRule getViolatedRule() {
    return this.violatedRule;
  }

  public String getMessage() {
    return this.message;
  }

}
