package com.qaiware.eventsourcing.eventstore.command.validation.rule;

import com.qaiware.eventsourcing.eventstore.command.validation.ValidationError;
import com.qaiware.eventsourcing.eventstore.event.Event;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class ValidationRuleResult {

  private List<Event> failEvents;

  private List<ValidationError> errors;

  private ValidationRuleResult() {

  }

  private ValidationRuleResult(List<ValidationError> errors) {
    this.failEvents = new ArrayList<>();

    errors.forEach(validationError -> this.failEvents.addAll(
        validationError.getViolatedRule().getFailEvents())
    );

    this.errors = errors;
  }

  public static ValidationRuleResult create() {
    return new ValidationRuleResult();
  }

  public static ValidationRuleResult create(ValidationError... errors) {
    return new ValidationRuleResult(Arrays.asList(errors));
  }

  public boolean emitsFailEvents() {
    return (this.failEvents != null && !this.failEvents.isEmpty());
  }

  public boolean containsErrors() {
    return this.errors != null;
  }

  public List<Event> getFailEvents() {
    return this.failEvents;
  }

  public List<ValidationError> getErrors() {
    return this.errors;
  }

}
