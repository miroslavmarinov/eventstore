package com.qaiware.eventsourcing.eventstore.model;

import java.time.Instant;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "es_aggregate")
public class ESAggregate {

  @Id
  @Column(name = "a_id", nullable = false)
  private UUID id;

  @Column(name = "a_created", nullable = false, updatable = false)
  private Instant created;

  @Column(name = "a_updated", nullable = false, updatable = false)
  private Instant updated;

  @Column(name = "a_version")
  @Version
  private Long version;

  private ESAggregate() {

  }

  public static ESAggregate create(UUID id) {
    ESAggregate aggregate = new ESAggregate();

    aggregate.setId(id);
    aggregate.setCreated(Instant.now());
    aggregate.setUpdated(Instant.now());
    aggregate.setVersion(1L);

    return aggregate;
  }

  public UUID getId() {

    return id;
  }

  public void setId(UUID id) {

    this.id = id;
  }

  public Instant getCreated() {

    return created;
  }

  public void setCreated(Instant created) {

    this.created = created;
  }

  public Instant getUpdated() {

    return updated;
  }

  public void setUpdated(Instant updated) {

    this.updated = updated;
  }

  public Long getVersion() {

    return version;
  }

  public void setVersion(Long version) {

    this.version = version;
  }
}
