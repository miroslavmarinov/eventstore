package com.qaiware.eventsourcing.eventstore.model;

import java.time.Instant;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "es_event")
public class ESEvent {

  // event_id, event_type, event_data, entity_type, entity_id, triggering_event, metadata
  @Id
  @Column(name = "e_id", nullable = false)
  private UUID id;

  @Column(name = "e_type", nullable = false)
  private String eventType;

  @Column(name = "e_aggregate_type", nullable = false)
  private String aggregateType;

  @Column(name = "e_aggregate_id", nullable = false)
  private UUID aggregateId;

  @Column(name = "e_created", nullable = false, updatable = false)
  private Instant created;

  @Column(name = "e_payload")
  private String payload;

  private ESEvent() {

  }

  public static ESEvent create(
      @NotNull String eventType,
      @NotNull String aggregateType,
      @NotNull UUID aggregateId,
      String payload
  ) {

    ESEvent event = new ESEvent();
    event.setId(UUID.randomUUID());
    event.setEventType(eventType);
    event.setAggregateType(aggregateType);
    event.setAggregateId(aggregateId);
    event.setPayload(payload);
    event.setCreated(Instant.now());
    return event;
  }

  public UUID getId() {

    return this.id;
  }

  protected void setId(UUID id) {

    this.id = id;
  }

  public String getEventType() {

    return this.eventType;
  }

  protected void setEventType(String eventType) {

    this.eventType = eventType;
  }

  public String getAggregateType() {

    return this.aggregateType;
  }

  protected void setAggregateType(String aggregateType) {

    this.aggregateType = aggregateType;
  }

  public UUID getAggregateId() {

    return this.aggregateId;
  }

  protected void setAggregateId(UUID aggregateId) {

    this.aggregateId = aggregateId;
  }

  public Instant getCreated() {

    return this.created;
  }

  protected void setCreated(Instant created) {

    this.created = created;
  }

  public String getPayload() {

    return this.payload;
  }

  protected void setPayload(String payload) {

    this.payload = payload;
  }
}
