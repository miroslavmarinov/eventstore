package com.qaiware.eventsourcing.eventstore.aggregate;

import com.qaiware.eventsourcing.eventstore.event.Event;
import java.util.UUID;
import org.springframework.stereotype.Component;

@Component
public class DummyEventPublisher implements EventPublisher {

  @Override
  public void publish(UUID aggregateId, Event event) {

    System.out.println(
        "Publishing event " + event.getClass().getSimpleName() + " to aggregate " + aggregateId
            .toString());
  }
}
