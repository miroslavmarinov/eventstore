package com.qaiware.eventsourcing.eventstore.aggregate;

public interface AggregateClassMapper {

  Class<? extends AbstractAggregate> getAggregateClassByType(String aggregateType);

  String getTypeByAggregateClass(Class<? extends AbstractAggregate> clazz);
}
