package com.qaiware.eventsourcing.eventstore.aggregate;

import com.qaiware.eventsourcing.eventstore.command.Command;
import com.qaiware.eventsourcing.eventstore.event.Event;
import com.qaiware.eventsourcing.eventstore.model.ESAggregate;
import com.qaiware.eventsourcing.eventstore.model.ESEvent;
import com.qaiware.eventsourcing.eventstore.repository.ESAggregateRepository;
import com.qaiware.eventsourcing.eventstore.repository.ESEventRepository;
import com.qaiware.eventsourcing.eventstore.util.JSONMapper;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional
public class EventStore {

  @Autowired
  private ESEventRepository esEventRepository;

  @Autowired
  private ESAggregateRepository esAggregateRepository;

  @Autowired
  private AggregateClassMapper aggregateClassMapper;

  public <T extends AbstractAggregate<T, C>, C extends Command> UUID persistEvent(
      Class<T> clazz,
      UUID aggregateId,
      Event event
  ) {

    ESEvent esEvent = ESEvent.create(
        event.getClass().getName(),
        this.aggregateClassMapper.getTypeByAggregateClass(clazz),
        aggregateId,
        JSONMapper.toJSON(event)
    );

    return this.esEventRepository.save(esEvent).getId();
  }

  public UUID createAggregate(
      UUID aggregateId
  ) {

    ESAggregate esAggregate = ESAggregate.create(aggregateId);

    return this.esAggregateRepository.save(esAggregate).getId();
  }

  public UUID updateAggregate(ESAggregate esAggregate) {

    return this.esAggregateRepository.save(esAggregate).getId();
  }

  public ESAggregate getAggregateById(UUID aggregateId) {
    return this.esAggregateRepository.getAggregateByIdForUpdate(aggregateId);
  }

  public List<Event> getAggregateEvents(UUID aggregateId) {

    return this.esEventRepository
        .findAllByAggregateId(aggregateId)
        .stream()
        .map(EventStore::toEvent)
        .collect(Collectors.toList());
  }

  private static Event toEvent(ESEvent esEvent) {

    try {
      return (Event) JSONMapper
          .fromJSON(esEvent.getPayload(), Class.forName(esEvent.getEventType()));
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
      throw new RuntimeException("Event deserialization failed.");
    }

  }

}
