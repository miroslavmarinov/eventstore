package com.qaiware.eventsourcing.eventstore.aggregate;

import com.qaiware.eventsourcing.eventstore.command.Command;
import com.qaiware.eventsourcing.eventstore.event.Event;
import java.lang.reflect.InvocationTargetException;
import java.util.UUID;

public abstract class AbstractAggregate<T extends AbstractAggregate<T, C>, C extends Command> {

  private UUID id;

  public T apply(Event event)
      throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {

    this.getClass().getMethod("apply", event.getClass()).invoke(this, event);
    return (T) this;
  }

  public Event process(C cmd)
      throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {

    return (Event) this.getClass().getMethod("process", cmd.getClass()).invoke(this, cmd);
  }

  public UUID getId() {

    return this.id;
  }

  public void setId(UUID id) {

    this.id = id;
  }

}
