package com.qaiware.eventsourcing.eventstore.aggregate;

import com.qaiware.eventsourcing.eventstore.event.Event;
import java.util.UUID;

public interface EventPublisher {

  void publish(UUID aggregateId, Event event);
}
