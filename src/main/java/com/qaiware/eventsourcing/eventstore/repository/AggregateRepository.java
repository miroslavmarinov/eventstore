package com.qaiware.eventsourcing.eventstore.repository;

import com.qaiware.eventsourcing.eventstore.aggregate.AbstractAggregate;
import com.qaiware.eventsourcing.eventstore.aggregate.EventPublisher;
import com.qaiware.eventsourcing.eventstore.aggregate.EventStore;
import com.qaiware.eventsourcing.eventstore.command.Command;
import com.qaiware.eventsourcing.eventstore.event.Event;
import com.qaiware.eventsourcing.eventstore.model.ESAggregate;
import java.lang.reflect.InvocationTargetException;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import javax.transaction.Transactional;

public class AggregateRepository<T extends AbstractAggregate<T, C>, C extends Command> {

  private final Class<T> clazz;
  private final EventStore eventStore;
  private final EventPublisher eventPublisher;

  private static class Key {

    private int leaseCount = 0;

    private int getLeaseCount() {

      return leaseCount;
    }

    private void increaseLeaseCount() {

      leaseCount++;
    }

    private void decreaseLeaseCount() {

      leaseCount--;
    }
  }

  private static HashMap<String, Key> keyChain = new HashMap<>();

  private static synchronized Key obtainLock(String key) {

    if (!keyChain.containsKey(key)) {
      keyChain.put(key.toString(), new Key());
    }
    Key keyObj = keyChain.get(key);
    keyObj.increaseLeaseCount();

    System.out.println("Key " + key + " leased " + keyObj.getLeaseCount() + " times");
    return keyChain.get(key);
  }

  private static synchronized void releaseLock(String key) {

    if (keyChain.containsKey(key)) {
      Key keyObj = keyChain.get(key);
      keyObj.decreaseLeaseCount();
      System.out.println("Releasing key " + key + ". " + keyObj.getLeaseCount() + " leases left.");
      if (keyObj.getLeaseCount() == 0) {
        keyChain.remove(key);
        System.out.println("Key " + key + " removed from hash. ");
      }
    }
  }

  public AggregateRepository(Class<T> clazz, EventStore eventStore, EventPublisher eventPublisher) {

    this.clazz = clazz;
    this.eventStore = eventStore;
    this.eventPublisher = eventPublisher;
  }

  public T create(C cmd) {

    T aggregate;
    try {
      aggregate = this.clazz.newInstance();
      Event event = aggregate.process(cmd);
      aggregate.apply(event);

      aggregate.setId(UUID.randomUUID());

      this.eventStore.createAggregate(aggregate.getId());

      this.eventStore.persistEvent(this.clazz, aggregate.getId(), event);

      this.eventPublisher.publish(aggregate.getId(), event);

      return aggregate;
    } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
      throw new RuntimeException(e);
    }
  }

  @Transactional
  public T update(UUID id, C cmd) {

    T aggregate;

    Key lock = obtainLock(id.toString());

    try {
      aggregate = this.clazz.newInstance();
      aggregate.setId(id);

      systemOut("Getting the aggregate " + id.toString() + " for update...");

      ESAggregate esAggregate = this.eventStore.getAggregateById(id);

      systemOut("Just got the aggregate " + id.toString());

      systemOut("Getting all events for aggregate " + id.toString());

      List<Event> events = this.eventStore.getAggregateEvents(id); //TODO

      systemOut("Just got the events for aggregate " + id.toString());

      if (events.isEmpty()) {
        throw new IllegalStateException(
            "Cannot updateEvent non-existing aggregate with id " + id.toString());
      }

      // Restore aggregate's current state, by replaying all events
      for (Event event : events) {
        aggregate.apply(event);
      }

      Event event = aggregate.process(cmd);

      systemOut("Taking a nap... ");

      try {
        Thread.sleep(10000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }

      systemOut("Just woke up.");

      aggregate.apply(event);

      systemOut("Persisting event for aggregate " + id.toString());

      this.eventStore.persistEvent(this.clazz, id, event);

      this.eventPublisher.publish(aggregate.getId(), event);

      systemOut("Updating aggregate " + id.toString());

      this.eventStore.updateAggregate(esAggregate);

      systemOut("Aurevoir! " + id.toString());

      return aggregate;
    } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
      throw new RuntimeException(e);
    } finally {
      releaseLock(id.toString());
    }
  }

  private void systemOut(String message) {

    System.out.println(
        Instant.now().toString() + " [" + Thread.currentThread().getName() + "] " + message);
  }
}
