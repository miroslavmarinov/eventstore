package com.qaiware.eventsourcing.eventstore.repository;

import com.qaiware.eventsourcing.eventstore.model.ESEvent;
import java.util.List;
import java.util.UUID;
import javax.persistence.LockModeType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ESEventRepository extends JpaRepository<ESEvent, String> {

  @Query("select e from ESEvent e where e.aggregateId = :aggregateId order by e.created ASC")
  List<ESEvent> findAllByAggregateId(@Param("aggregateId") UUID aggregateId);

}
