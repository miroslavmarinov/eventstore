package com.qaiware.eventsourcing.eventstore.repository;

import com.qaiware.eventsourcing.eventstore.model.ESAggregate;
import com.qaiware.eventsourcing.eventstore.model.ESEvent;
import java.util.List;
import java.util.UUID;
import javax.persistence.LockModeType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ESAggregateRepository extends JpaRepository<ESAggregate, UUID> {

  @Query("select a from ESAggregate a where a.id = :id")
  @Lock(LockModeType.PESSIMISTIC_WRITE)
  ESAggregate getAggregateByIdForUpdate(@Param("id") UUID id);

}
