package com.qaiware.eventsourcing.service;

import com.qaiware.eventsourcing.api.json.CreateTransactionRequest;
import com.qaiware.eventsourcing.api.json.CreditTransactionRequest;
import com.qaiware.eventsourcing.api.json.DebitTransactionRequest;
import com.qaiware.eventsourcing.api.json.TransactionResponse;
import com.qaiware.eventsourcing.domain.aggregates.TransactionAggregate;
import com.qaiware.eventsourcing.domain.commands.CreateTransactionCommand;
import com.qaiware.eventsourcing.domain.commands.CreditTransactionCommand;
import com.qaiware.eventsourcing.domain.commands.DebitTransactionCommand;
import com.qaiware.eventsourcing.eventstore.command.Command;
import com.qaiware.eventsourcing.eventstore.repository.AggregateRepository;
import java.math.BigDecimal;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TransactionService {

  @Autowired
  private AggregateRepository<TransactionAggregate, Command> aggregateRepository;

  public TransactionResponse create(CreateTransactionRequest createTransactionRequest) {

    CreateTransactionCommand cmd = CreateTransactionCommand.create(
        new BigDecimal(createTransactionRequest.getInitialAmount())
    );

    TransactionAggregate aggregate = this.aggregateRepository.create(cmd);

    return TransactionResponse.create(
        aggregate.getId(),
        aggregate.getAmount()
    );
  }

  public TransactionResponse credit(String transactionId,
      CreditTransactionRequest creditTransactionRequest) {

    CreditTransactionCommand cmd = CreditTransactionCommand.create(
        new BigDecimal(creditTransactionRequest.getAmount())
    );

    TransactionAggregate aggregate = this.aggregateRepository
        .update(UUID.fromString(transactionId), cmd);

    return TransactionResponse.create(
        aggregate.getId(),
        aggregate.getAmount()
    );
  }

  public TransactionResponse debit(String transactionId,
      DebitTransactionRequest debitTransactionRequest) {

    DebitTransactionCommand cmd = DebitTransactionCommand.create(
        new BigDecimal(debitTransactionRequest.getAmount())
    );

    TransactionAggregate aggregate = this.aggregateRepository
        .update(UUID.fromString(transactionId), cmd);

    return TransactionResponse.create(
        aggregate.getId(),
        aggregate.getAmount()
    );
  }
}
